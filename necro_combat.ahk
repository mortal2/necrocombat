#persistent
global timercount, countdown

; --------------------------------------------------------------------------------------------------------
; Your hotkeys should be here
; --------------------------------------------------------------------------------------------------------
global DH_HOTKEY := 3
global AUWSURGE_HOTKEY := "G"
global CANCEL_HOTKEY := "{Ctrl}"
global SELFCAST_HOTKEY := "Right"
; --------------------------------------------------------------------------------------------------------


spellTimer(x){
    timercount := 0 
    countdown := x
    WinSet, TransColor, FFFFFF 255, Countdown
    Progress,1: m0 b0 fs7 fm10 zh7 CTgreen CWFFFFFF W90 P100, % SubStr("00" floor((countdown-timercount)/60),-1) ":" SubStr("00" mod(countdown-timercount,60),-1),, Countdown
    settimer,CountdownProgressBar,100

        CountdownProgressBar:
            timercount+=100
            if (timercount < countdown)
            {
                Progress, % "1:" 100*(countdown-timercount)/countdown, .
                ;RecallCooldown = 1
            }
            else if (timercount = countdown)
            {	
                SetTimer CountdownProgressBar, off
                Progress,1: Off
            }
            WinSet, TransColor, FFFFFF 255, Countdown
        Return
}

AuwSurge()
{
    Send +%AUWSURGE_HOTKEY%
    Sleep 50 ;  milliseconds
    Send %CANCEL_HOTKEY% ; Cencel
    Sleep 50 ;
    Click, %SELFCAST_HOTKEY% ; replace with send if needed
    spellTimer(4000)
}
; --------------------------------------------------------------------------------------------------------
; Macro management
; --------------------------------------------------------------------------------------------------------
; Reload macro
F5::Reload

; Cast DH, single button
F1::
    Send %DH_HOTKEY%
    spellTimer(3200) ; in milliseconds
    return

; Cast DH while running, shift + single button
+F1::
    Send %DH_HOTKEY%
    spellTimer(3200)
    return

; Cast Auw, single button
F2::
    AuwSurge()
    return

; Cast Auw while running, shift + single button
+F2::
    AuwSurge()
    return
; --------------------------------------------------------------------------------------------------------
